<script src="<?php echo base_url(); ?>assetsadmin/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/datapicker/bootstrap-datepicker.js"></script>


    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url(); ?>assetsadmin/js/inspinia.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/pace/pace.min.js"></script>
    <script type="text/javascript" src="assets/tinymce/tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/gritter/jquery.gritter.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/demo/sparkline-demo.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/toastr/toastr.min.js"></script>

    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/dataTables/dataTables.tableTools.min.js"></script>
