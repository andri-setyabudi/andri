<!DOCTYPE html>
<html lang="en">
<head>
    <title>Data User</title>
    </head>
<body>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Laporan A</h1>
            </div>
        </div>
        <?=$this->session->flashdata('pesan')?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                              <th>No</th>
                                <th>Nama</th>
                                <th>Umur</th>
                            </tr>
                            </thead>
                            <?php $no=1;
                            foreach ($customer as $data) { ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $no; ?></td>
                                <td><?php echo $data->name; ?></td>
                                <td>
                                    <?php echo $data->age; ?>
                                </td>
                                
                            </tr>
                            </tbody>
                            <?php $no++; } ?>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- laporan b -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Laporan B</h1>
            </div>
        </div>
        <?=$this->session->flashdata('pesan')?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                              <th>No</th>
                                <th>Nama</th>
                                <th>Umur</th>
                            </tr>
                            </thead>
                            <?php $no=1;
                            foreach ($customerb as $data) { ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $no; ?></td>
                                <td><?php echo $data->name; ?></td>
                                <td>
                                    <?php echo $data->age; ?>
                                </td>
                                
                            </tr>
                            </tbody>
                            <?php $no++; } ?>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- laporan c -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Laporan C</h1>
            </div>
        </div>
        <?=$this->session->flashdata('pesan')?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                              <th>No</th>
                                <th>Nama</th>
                                <th>Order</th>
                            </tr>
                            </thead>
                            <?php $no=1;
                            foreach ($customerc as $data) { ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $no; ?></td>
                                <td><?php echo $data->name; ?></td>
                                <td>
                                    <?php echo $data->jml; ?>
                                </td>
                                
                            </tr>
                            </tbody>
                            <?php $no++; } ?>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog">
                        <form method="post" action="<?=base_url(); ?>user/insert_user">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Tambah User</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control" placeholder="username" required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="email" required>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input class="form-control" placeholder="alamat" name="alamat" required>
                                </div>
                                <div class="form-group">
                                    <label>password</label>
                                    <input type="password" class="form-control" placeholder="password" name="password" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>assetsadmin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/bootstrap.min.js"></script>
   
       <script type="text/javascript">
            $(function() {
                $("#tble").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
</body>
</html>