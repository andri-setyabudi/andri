<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			redirect('auth/login');
		}
	}

 function index(){
		$this->load->view('admin/header');
		$data['customer'] = $this->m_web->customer();
		$data['customerb'] = $this->m_web->customerb();
		$data['customerc'] = $this->m_web->customerc();
		$this->load->view('admin/customer', $data);

	}

	
}