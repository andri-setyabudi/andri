<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		$pass = $this->valid_pass($this->input->post('password', TRUE));
		// echo $pass; die();
		if (!$pass) {
			$this->session->set_flashdata("pesan",
				"<div class=\"alert alert-danger\">
					<small>Minimal Satu Huruf Besar dan Kecil.</small>
				</div>");
			die(redirect('auth/login', $error));
		}

		if($pass){

		$data = array (
		'username' => $this->input->post('username', TRUE),
		'password' => md5($this->input->post('password', TRUE))
		);

		$hasil = $this->m_web->user($data);

		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = TRUE;
				$sess_data['username'] = $sess->username;
				$sess_data['password'] = $sess->password;

				$this->session->set_userdata($sess_data);
			}
			// berhasil
			redirect(site_url());
		}else{
			// error
			$this->session->set_flashdata("pesan",
				"<div class=\"alert alert-danger\">
					<small>Terjadi kegagalan sistem.</small>
				</div>");
			redirect('auth/login', $error);
		}


		

	}else{
		// $error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata("pesan",
				"<div class=\"alert alert-danger\">
					<small>Invalid Username Or Password.</small>
				</div>");
			redirect('auth/login', $error);
	}

}
	public function logout()
	{
	
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');
		$this->session->unset_userdata('logged_in');
		$this->cart->destroy();
		redirect('auth/login');
	}
	public function login_administrator()
	{
		$this->load->view('adminis/login');
	}
	public function login()
	{
		
		$this->load->view('admin/login/login');

	}
	public function register()
	{
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$gender = $_POST['gender'];
		$date_of_birth = $_POST['date_of_birth'];
		$email = $_POST['email'];
		$email_confirmation = $_POST['email_confirmation'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$password_confirmation = $_POST['password_confirmation'];
		$level = $_POST['level'];
		$data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'gender' => $gender,
			'date_of_birth' => $date_of_birth,
			'email' => $email,
			'email_confirmation' => $email_confirmation,
			'username' => $username,
			'password' => md5($password),
			'password_confirmation' => md5($password_confirmation),
			'level' => $level
		);
		$res = $this->db->insert('user', $data);
		if($res>=1){
			redirect('auth/thank_you');
		}else{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata("notif", 
				"<div class=\"alert alert-danger\" id=\"alert\">
				Registration failed.
				</div>");
			redirect('auth/login', $error);
		}
	}
public	function valid_pass($candidate) { 
	$r1='/[A-Z]/'; //Uppercase 
	$r2='/[a-z]/'; //lowercase 
	$r4='/[0-9]/'; //numbers 
	if(preg_match_all($r1,$candidate, $o)<1) return FALSE; 
	if(preg_match_all($r2,$candidate, $o)<1) return FALSE; 
	if(preg_match_all($r4,$candidate, $o)<1) return FALSE; 

	if(strlen($candidate)<8) return FALSE; 
	return TRUE; 
}

}