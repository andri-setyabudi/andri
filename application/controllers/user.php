<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			redirect('auth/login');
		}
	}

 function index(){
		$this->load->view('admin/header');
		$data['user'] = $this->m_web->datauser();
		$this->load->view('admin/user', $data);
	}
	
public function edit_user($id)
	{
		$this->load->view('admin/header');
		$data['user'] = $this->m_web->datauser();
		$data['where'] = $this->m_web->where_user($id);
		$this->load->view('admin/edit_user', $data);
	}
	

 public function update_user()
	{
		$id_user= $_POST['id_user'];
		$username = $_POST['username'];
		$alamat = $_POST['alamat'];
		
		
		$data = array(
			'user_id' => $id_user,
			'username' => $username,
			'address' => $alamat,
			
		);

		$where = array('user_id' => $id_user);
		$res = $this->m_web->update('tbl_user',$data, $where);
		if($res>=1){
			$this->session->set_flashdata("pesan", 
				"<div class=\"alert alert-info\" id=\"alert\">
				Success Update User.
				</div>");
			redirect('user/'.$id_produk);
		}else{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\">
			Failed Update User.
			</div>");
			redirect('admin/edit_product/'.$id_produk, $error);
		}
	}

public function insert_user()
	{
		$username = $_POST['username'];
		$email= $_POST['email'];
		$password = $_POST['password'];
		$alamat = $_POST['alamat'];

		$data = array(
			'username' => $username,
			'email' => $email,
			'address' => $alamat,
			'password' => md5($password),
		);
		$res = $this->db->insert('tbl_user', $data);

		if($res>=1){
			$this->session->set_flashdata("pesan", 
				"<div class=\"alert alert-info\" id=\"alert\">
				Success Added User.
				</div>");
			redirect('user/');
		}else{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\">
			Failed Added User.
			</div>");
			redirect('user/', $error);
		}
	}	
function delete_user($id)
	{
		$where = array('user_id' => $id);
		$res = $this->m_web->delete('tbl_user', $where);
		if ($res>=1) {
			$this->session->set_flashdata("pesan", 
				"<div class=\"alert alert-info\" id=\"alert\">
				Delete Success.
				</div>");
			redirect('user/');
		}else{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata("pesan", 
				"<div class=\"alert alert-danger\" id=\"alert\">
				Delete Failed.
				</div>");
			redirect('user/', $error);
		}
	}
	
}