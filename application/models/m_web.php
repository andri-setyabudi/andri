<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_web extends CI_Model {

public function user($data)
	{
		$query = $this->db->get_where('tbl_user', $data);
		return $query;
	}
public function sale() {
		$this->db->select('*');
		$this->db->from('tblcustomer');
		$query = $this->db->get();
		return $query->result();
	}
public function customer() {
		$this->db->select('p.*');
		$this->db->from('tblorder o');
		$this->db->join('tblsalesperson p','o.salesperson_id = p.id', 'left');
		$this->db->join('tblcustomer c','c.id = o.cust_id');
		$this->db->where('c.name','samuel');
		$query = $this->db->get();
		return $query->result();
	}
public function customerb() {
		$this->db->select('*');
		$this->db->from('tblorder o');
		$this->db->join('tblsalesperson p','o.salesperson_id = p.id', 'left');
		$this->db->join('tblcustomer c','c.id = o.cust_id');
		$this->db->group_by('c.`name`');
		$this->db->where('c.name <>','samuel');
		$query = $this->db->get();
		return $query->result();
	}

public function customerc() {
$query = $this->db->query('SELECT COUNT(o.`number`) AS jml,p.`name`,p.`age` FROM tblorder o LEFT JOIN `tblsalesperson` p ON o.`salesperson_id`=p.`id`
LEFT JOIN tblcustomer c ON c.`id`=o.`cust_id`
GROUP BY o.`salesperson_id`');
	 		return $query->result();
	}



public function datauser() {
		$this->db->select('*');
		$this->db->from('tbl_user');
		$query = $this->db->get();
		return $query->result();
	}
public function where_user($where="") {
		$this->db->select('*');
		$this->db->from('tbl_user');
		
		$this->db->where('user_id', $where);
		$query = $this->db->get();
		return $query->result();
	}
public function update($tabelName,$data,$where) {
		$res = $this->db->update($tabelName,$data, $where);
		return $res;
	}
public function delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}
	
}